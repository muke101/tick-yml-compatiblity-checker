# tick-yml-compatiblity-checker
Visa use Influxdb and the TICK stack to monitor vital system health and send alerts when certain threshholds are reached - for instance if a production server passes 90% CPU usage, the responsible people are automatically emailed. 

To creat the scripts (.tick scripts) that define these alerts and what to do (at <x> send email to <person>), yaml scripts can be used to only require a simple template script and then create as many new alerts as needed using different values as defines in the yaml scripts where appropiate. 
However, while there exists syntax checkers for .tick scripts that come with the TICK stack, and obviously syntax checkers for yaml too, there's nothing that can parse and check against the two scripts (.yml and .tick) being compatible. 
For instance, a yaml script could define a variable for CPU usage percent as a string, while the tick script expects the automatically inserted value to be an integer, and nothing would detect this, so the alert would simply not work as expected.

This script exists to tackle this problem by parsing both scripts given and comparing all variable definitions that are to be inserted into the tick script, and returns mismatches, thus helping to further automate the process of alert creation for systems health monitoring.
