package main

import (
		"gopkg.in/yaml.v2"
		"text/scanner"
		"os"
		"fmt"
		)


func fileReader(fileName string) *os.File	{
	src, err := os.Open(fileName)
	if err != nil	{
		panic(err)
	}
	return src
}

func (s scanner.Scanner) getIndent(tok rune, text string) int {
	indent := 0
    for ;text == "\n" && tok != scanner.EOF; tok, text = s.Scan(), s.TokenText(){}
    for ;text == " "  && tok != scanner.EOF; tok, indent, text = s.Scan(), indent + 1, s.TokenText(){}
    indent /= 2
	return indent
}

func tokenizer(src *os.File) (chan []string)	{

	block := make(chan []string)

	go func()	{

		var s scanner.Scanner
		s.Init(src)
		s.Whitespace ^= 1<<'\t' | 1<<'\n' | 1<<' '
		var baseIndent int
		lines := 0
		var scannedBlock []string

		for tok, text := s.Scan(), s.TokenText(); tok != scanner.EOF; tok, text = s.Scan(), s.TokenText()	{

			if text == "#"	{ //ignore commented rest of lines
				for ;text != "\n" && tok != scanner.EOF; tok, text = s.Scan(), s.TokenText(){}
				//TODO: implement for tabs and tab-space mixes
			}	else if text == "\n"	{ //determine indent level of every new line
					baseIndent = s.getIndent(tok, text)
			}	else	{ //string literal, start of new block
				var indent int
				for ;indent != baseIndent && tok != scanner.EOF; tok, text = s.Scan(), s.TokenText()	{
					if text == "\n"	{
						indent = s.getIndent(tok, text)
					}
					scannedBlock = append(scannedBlock, text)
				}
				block <- scannedBlock
			}

		}

		close(block)

	}()

	return block

}



//TODO check why some comments aren't being stripped and what's going on with some indents (3)
func main()	{
	for i := range tokenizer	{
		fmt.Println(i)
	}
}
